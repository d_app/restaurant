package com.hababk.restaurant.utils;

/**
 * Created by Tayeb-Ali on 01-03-2019.
 */

public class Constants {
    public static final String KEY_TOKEN = "ApiToken";
    public static final String KEY_CATEGORY = "MenuItemCategory";
    public static final String KEY_USER = "StoreUser";
    public static final String KEY_BANK_DETAIL = "BankDetail";
    public static final String KEY_PROFILE = "ChefProfile";
    public static final String KEY_SETTING = "Setting";
    public static final String KEY_REFRESH_ITEMS = "RefreshItems";
    public static final String KEY_REFRESH_ORDERS = "RefreshOrders";
}
