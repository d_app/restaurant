package com.hababk.restaurant.network.request;

/**
 * Created by Tayeb-Ali on 01-03-2019.
 */

public class MobileVerifiedRequest {
    private String mobile_number;

    public MobileVerifiedRequest(String mobile_number) {
        this.mobile_number = mobile_number;
    }
}
