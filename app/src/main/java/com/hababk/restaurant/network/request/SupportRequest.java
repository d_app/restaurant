package com.hababk.restaurant.network.request;

/**
 * Created by Tayeb-Ali on 14-03-2018.
 */

public class SupportRequest {
    private String name, email, message;

    public SupportRequest(String name, String email, String message) {
        this.name = name;
        this.email = email;
        this.message = message;
    }
}
